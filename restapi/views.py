from django.contrib.auth.models import User, Group
from homepage.models import PDF
from rest_framework import viewsets
from .serializers import PDFSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework import viewsets, mixins
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.exceptions import NotFound
from django.db.models import Q
import functools
import operator

class PDFViewSet(mixins.RetrieveModelMixin,
                 mixins.ListModelMixin, 
                 viewsets.GenericViewSet):
    queryset = PDF.objects.all()
    serializer_class = PDFSerializer

    def list(self, request):
        permission_classes = (AllowAny,)
        serializer_class = PDFSerializer
        queryset = PDF.objects.all()

        req_search = request.query_params.get('search', False)
        if req_search:
            search_list = req_search.split(',')
            print("Search list: " + str(search_list))

            serializer = PDFSerializer(self.queryset.filter(functools.reduce(operator.and_, ((Q(title__icontains=x) | Q(faculty__icontains=x)) for x in search_list))),many=True, context={'request': request})

            if serializer.data:
                return Response(serializer.data)
            else:
                raise NotFound(detail="No such PDF found", code=404)
        else:
            serializer = PDFSerializer(queryset, many=True, context={'request': request})
            if serializer.data:
                return Response(serializer.data)
            else:
                raise NotFound(detail="No PDFs currently!", code=404)
       