from homepage.models import PDF
from rest_framework import serializers

class PDFSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = PDF
		fields = ('title','pdf_type','pdf_link','faculty','last_modified','size','date_created')