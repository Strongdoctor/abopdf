from django.db import models

class PDF(models.Model):
	title = models.CharField(max_length=255)
	pdf_type = models.CharField(max_length=255, null=True)
	pdf_link = models.CharField(max_length=512, null=True)
	faculty = models.CharField(max_length=255, null=True)
	last_modified = models.DateTimeField()
	size = models.CharField(max_length=32)
	date_created = models.DateTimeField(auto_now_add=True)