from django.shortcuts import render
import requests
from lxml import html
from .models import PDF
from datetime import date
from django.db.models import Q

def homepage(request):
    if request.method == 'GET':
        results = PDF.objects.all()
        return render(request, 'homepage/home.html', {'results': results, 'searched': False})
    elif request.method == 'POST':
        search_input = request.POST.get("pdf_search_field", "").split(" ")

        q = Q()
        for term in search_input:
            q |= Q(title__icontains = term)
            q |= Q(faculty__icontains = term)

        results = PDF.objects.filter(q)

        return render(request, 'homepage/home.html', {'results': results, 'searched': True})