import requests
import datetime
from django_cron import CronJobBase, Schedule
from lxml import html
from .models import PDF
from pytz import timezone
import urllib

class ScrapePDFs(CronJobBase):
    RUN_EVERY_MINS = 60 # every 1 hour

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'homepage.scrape_pdfs'    # a unique code

    def do(self):
        print("Running CRON: ScrapePDFs")

        # Trailing slash is REQUIRED
        list_of_up_urls = [
            ('http://web.abo.fi/up/FNT/', 'FNT'),
            ('http://web.abo.fi/up/FHPT/', 'FHPT'),
            ('http://web.abo.fi/up/FPV/', 'FPV'),
            ('http://web.abo.fi/up/FSE/', 'FSE'),
        ]

        for url in list_of_up_urls:
            try:
                self.get_up_pdfs_in_folder(url[0], "Undervisningsplan", url[1])
            except Exception as e:
                print("ERROR IN: list_of_up_urls " + str(url))
                print(e)

        list_of_studieschema_urls = [
            ('http://web.abo.fi/studiehandbok/it/2017-2018/Studiegang/', 'FNT'),
            ('http://web.abo.fi/studiehandbok/biovet/2017-2018/Studiegang/', 'FNT'),
            ('http://web.abo.fi/studiehandbok/ekon/studiegang/amnenas%20studiegangar/2017-2018/', 'FSE'),
            ('http://web.abo.fi/studiehandbok/fpv/valfard/studiegang/2014-2015/', 'FPV'),
            ('http://web.abo.fi/studiehandbok/hf/2017-2018/studieschema/', 'FHPT'),
            ('http://web.abo.fi/studiehandbok/samhvetabo/studiegang/amnenas%20studiegangar/2017-2018/', 'FSE'),
        ]

        for url in list_of_studieschema_urls:
            try:
                self.get_studieschema_pdfs_in_folder(url[0], "Studieschema", url[1])
            except Exception as e:
                print("ERROR IN: list_of_studieschema_urls " + str(url))
                print(e)
    
    def get_up_pdfs_in_folder(self, url: str, pdf_type: str, faculty: str):
        page = requests.get(url)
        tree = html.fromstring(page.content)

        allish = tree.xpath('//tr')
        del allish[0:3]

        pdfs = list()

        for x in range(0, len(allish)-1):
            tds = allish[x].xpath('td')
            pdf_title = tds[1].xpath('a/@href')[0].strip()
            if pdf_title.split(".")[-1] == "pdf":
                pdf_link = url + pdf_title
                pdf_last_modifed = tds[2].xpath('text()')[0].strip()
                pdf_size = tds[3].xpath('text()')[0].strip()
                helsinki_tz = timezone('Europe/Helsinki')

                pdfs.append((
                        urllib.parse.unquote(pdf_title).split(".pdf")[0].replace("_"," "),
                        pdf_type,
                        pdf_link,
                        faculty,
                        helsinki_tz.localize(datetime.datetime.strptime(pdf_last_modifed, '%d-%b-%Y %H:%M')),
                        pdf_size
                ))

        print(pdfs)

        for pdf in pdfs:
            obj, created = PDF.objects.update_or_create(
                title = pdf[0],
                defaults = {'title': pdf[0], 'pdf_type': pdf[1], 'pdf_link': pdf[2], 'faculty': pdf[3], 'last_modified': pdf[4], 'size': pdf[5]}
            )

    def get_studieschema_pdfs_in_folder(self, url: str, pdf_type: str, faculty: str):
        page = requests.get(url)
        tree = html.fromstring(page.content)

        allish = tree.xpath('//tr')
        del allish[0:3]

        pdfs = list()

        for x in range(0, len(allish)-1):
            tds = allish[x].xpath('td')
            pdf_title = tds[1].xpath('a/@href')[0].strip()
            if pdf_title.split(".")[-1] == "pdf":
                pdf_link = url + pdf_title
                pdf_last_modifed = tds[2].xpath('text()')[0].strip()
                pdf_size = tds[3].xpath('text()')[0].strip()
                helsinki_tz = timezone('Europe/Helsinki')

                pdfs.append((
                        urllib.parse.unquote(pdf_title).split(".pdf")[0].replace("_"," "),
                        pdf_type,
                        pdf_link,
                        faculty,
                        helsinki_tz.localize(datetime.datetime.strptime(pdf_last_modifed, '%d-%b-%Y %H:%M')),
                        pdf_size
                ))

        print(pdfs)

        for pdf in pdfs:
            obj, created = PDF.objects.update_or_create(
                title = pdf[0],
                defaults = {'title': pdf[0], 'pdf_type': pdf[1], 'pdf_link': pdf[2], 'faculty': pdf[3], 'last_modified': pdf[4], 'size': pdf[5]}
            )
