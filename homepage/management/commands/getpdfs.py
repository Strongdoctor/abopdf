#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import datetime
from django.core.management.base import BaseCommand, CommandError
from django_cron import CronJobBase, Schedule
from lxml import html
from homepage.models import PDF
from pytz import timezone
from django.db import transaction
import urllib

class Command(BaseCommand):
    help = "Fetches the PDFs"

    def handle(self, *args, **options):
        print("Running CRON: ScrapePDFs")

        list_of_up_urls = [
            ('http://web.abo.fi/up/FNT', 'FNT'),
            ('http://web.abo.fi/up/FHPT', 'FHPT'),
            ('http://web.abo.fi/up/FPV', 'FPV'),
            ('http://web.abo.fi/up/FSE', 'FSE'),
        ]

        list_of_studieschema_urls = [
            ('http://web.abo.fi/studiehandbok/it/2017-2018/Studiegang', 'FNT'),
            ('http://web.abo.fi/studiehandbok/biovet/2017-2018/Studiegang', 'FNT'),
            ('http://web.abo.fi/studiehandbok/ekon/studiegang/amnenas%20studiegangar/2017-2018', 'FSE'),
            ('http://web.abo.fi/studiehandbok/fpv/valfard/studiegang/2014-2015', 'FPV'),
            ('http://web.abo.fi/studiehandbok/hf/2017-2018/studieschema', 'FHPT'),
            ('http://web.abo.fi/studiehandbok/samhvetabo/studiegang/amnenas%20studiegangar/2017-2018', 'FSE'),
        ]

        list_of_amnesstruktur_urls = [
            ('http://web.abo.fi/studiehandbok/ekon/amnesstruktur/2017-2018', 'FSE'),
        ]

        list_of_examensstruktur_urls = [
            ('http://web.abo.fi/studiehandbok/biovet/2017-2018/Examensstrukturer', 'FNT'),
            ('http://web.abo.fi/studiehandbok/ekon/examensstruktur', 'FSE'),
            ('http://web.abo.fi/studiehandbok/it/2017-2018/Examensstrukturer', 'FNT'),
        ]

        list_of_ospecifierad_urls = [
            ('http://web.abo.fi/studiehandbok/kt/2017-2018/', 'FNT')
        ]

        self.handle_url_list(list_of_up_urls, "Undervisningsplan")

        self.handle_url_list(list_of_studieschema_urls, "Studieschema")

        self.handle_url_list(list_of_amnesstruktur_urls, "Ämnesstruktur")

        self.handle_url_list(list_of_examensstruktur_urls, "Examensstruktur")

        self.handle_url_list(list_of_ospecifierad_urls, "Ospecifierad")

    def handle_url_list(self, list_of_urls: list, pdf_type: str):
        for url in list_of_urls:
            try:
                nr_of_pdfs_fetched = self.get_pdfs_in_folder(url[0], pdf_type, url[1])
                self.stdout.write(
                    self.style.SUCCESS('Fetched ' 
                        + str(nr_of_pdfs_fetched) 
                        + ' of the type \"' 
                        + pdf_type 
                        + '\" from URL \"' 
                        + url[0].split('://')[1]
                        + '\"'
                    )
                )

            except Exception as e:
                print("ERROR IN: list_of_up_urls " + str(url))
                print(e)

    def get_pdfs_in_folder(self, url: str, pdf_type: str, faculty: str) -> int:

        # Append forward slash if not present in the list of urls
        if not url.endswith('/'):
            url = url + '/'

        page = requests.get(url)
        tree = html.fromstring(page.content)

        allish = tree.xpath('//tr')
        del allish[0:3]

        pdfs = list()

        for x in range(0, len(allish)-1):
            tds = allish[x].xpath('td')
            pdf_title = tds[1].xpath('a/@href')[0].strip()
            if pdf_title.split(".")[-1] == "pdf":
                pdf_link = (url + pdf_title)
                pdf_last_modifed = tds[2].xpath('text()')[0].strip()
                pdf_size = tds[3].xpath('text()')[0].strip()
                helsinki_tz = timezone('Europe/Helsinki')

                pdfs.append((
                        urllib.parse.unquote(pdf_title, encoding='utf-8').split(".pdf")[0].replace("_"," "),
                        pdf_type,
                        pdf_link,
                        faculty,
                        helsinki_tz.localize(datetime.datetime.strptime(pdf_last_modifed, '%d-%b-%Y %H:%M')),
                        pdf_size
                ))

        for pdf in pdfs:
            with transaction.atomic():
                obj, created = PDF.objects.update_or_create(
                    title = pdf[0],
                    defaults = {'title': pdf[0], 'pdf_type': pdf[1], 'pdf_link': pdf[2], 'faculty': pdf[3], 'last_modified': pdf[4], 'size': pdf[5]}
                )

        return len(pdfs)