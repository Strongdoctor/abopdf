# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-26 11:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0002_pdf_pdf_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='pdf',
            name='faculty',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
