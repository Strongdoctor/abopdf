$(document).ready(function(){
    $('#customsearchinput').val("");
    $('#result-table').DataTable({
    	"order": [[ 0, 'asc' ]],
    	"language": {
    		search: "Sök:",
    		info: "Visar filerna _START_ till _END_ av totalt _TOTAL_",
    		lengthMenu: "Visa _MENU_ filer per sida",
    		infoEmpty: "Visar filerna 0 till 0, av 0 hittade",
    		infoFiltered: "(filtrerat från totalt 122 filer)",
    		zeroRecords: "Hittade inga sådana filer",
    		loadingRecords: "Laddar listan..",
    		paginate: {
    			first: "Första",
    			previous: "Föregående sida",
    			next: "Nästa sida",
    			last: "Sista",
    		}
    	},
    });

    const oTable = $('#result-table').DataTable();   //pay attention to capital D
   
    $('#customsearchinput').keyup(function(){
          oTable.search($(this).val()).draw() ;
    });

    const resetsearchfield = document.getElementById("resetsearchfield");
    resetsearchfield.addEventListener("click",function(e){
        $('#customsearchinput').val("");
        oTable.search("").draw();
    },false);

    const mobileSort = $('#mobilesort select');
    mobileSort.on("change", function() {
        sortMobile($(this), oTable);
    });

    $("#result-table thead th").click(function() {
        var clickText = $(this).text();
        var sortDirection = $(this).attr('aria-sort');
        var optionIndex = getOptionIndex(clickText, sortDirection);

        var option = mobileSort.get(0).options[optionIndex];
        option.selected = true;
    });

    function sortMobile(triggerElement, oTable) {
        var value = triggerElement.val();
        var splitValue = value.split("_");

        oTable.order([getColumnIndex(splitValue[0]), splitValue[1]]);
        oTable.draw();
    }

    function getColumnIndex(columnName) {
        columnName = columnName.toLowerCase();
        if (columnName == "filnamn") {
            return 0;
        } else if (columnName == "fakultet") {
            return 1;
        } else if (columnName == "typ") {
            return 2;
        } else if (columnName == "uppdaterad") {
            return 3;
        }
    }

    function getOptionIndex(columnName, sortDirection) {
        const columnIndex = getColumnIndex(columnName);

        var optionValue = 0;
        for(x = 0; x < columnIndex; x++) {
            optionValue += 2;
        }

        if (sortDirection == "descending") {
            optionValue += 1;
        }

        return optionValue;
    }
});