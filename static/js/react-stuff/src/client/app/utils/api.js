export default function api_fetch() {
    const urlToFetch = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? 'http://localhost:8000/api/pdfs/' : 'https://abopdf.awest.io/api/pdfs/';
                
    return fetch(urlToFetch)
    .then(results => {
        return results.json();
    }).then(data => {
        let pdfs = data.map((pdf, idx) => {
            console.log("Pdf:",pdf)
            const date = new Date(pdf.last_modified);
            const year = date.getFullYear();
            const month = (date.getMonth() + 1).toString().length === 1 ? "0" + ((1 + date.getMonth()).toString()) : (1 + date.getMonth()).toString()
            console.log("Month:",month)
            const day = date.getDate().toString().length === 1 ? "0" + date.getDate().toString() : date.getDate().toString()
            console.log("Day:",date.getDay())
            console.log("Day length:",date.getDay().toString().length)
            const hour = (date.getHours() + 1).toString().length === 1 ? "0" + ((1 + date.getHours()).toString()) : (1 + date.getHours()).toString()
            console.log("Hours:",hour)
            const minute = (date.getMinutes() + 1).toString().length === 1 ? "0" + ((1 + date.getMinutes()).toString()) : (1 + date.getMinutes()).toString()

            console.log("Date:",date)

            const formatted_date = year + "-" + month + "-" + day + " " + hour + ":" + minute;

            return{
                pdf_link: pdf.pdf_link,
                title: pdf.title,
                faculty: pdf.faculty,
                pdf_type: pdf.pdf_type,
                date: formatted_date,
            }
        })
        pdfs.sort(function(a,b) {return (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0);});
        console.log("RETURNING PDFS:");
        console.log(pdfs);
        return pdfs;
    })
}