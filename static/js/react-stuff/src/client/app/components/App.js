import React from 'react'
import PdfTable from './PdfTable'

class App extends React.Component {
  render () {
    return <PdfTable />;
  }
}

export { App };