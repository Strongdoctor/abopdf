import React from 'react'

export function PdfTableRow(props) {
    return (
        <tr key={props.idx}>
            <td><a href={ props.pdf_link }>{props.title}</a></td>
            <td>{ props.faculty }</td>
            <td>{ props.pdf_type }</td>
        </tr>
    )
}