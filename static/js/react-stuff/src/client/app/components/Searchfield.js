import React from 'react'
import {render} from 'react-dom'

class Searchfield extends React.Component {
    constructor() {
        super();
        this.state = {
            timeout: 0,
        }
    }

    handleSearchUpdate = (event) => {
        var searchText = event.target.value;

        if(this.state.timeout) { clearTimeout(this.state.timeout) };

        this.state = ({
            typing: false,
            timeout: setTimeout(() => {
                this.sendToParent(searchText);
            }, 150)
        })
    };

    sendToParent = (searchText) => {
        this.props.callbackFromParent(searchText);
    }

    handleResetSearchField = () => {
        this.refs.searchfield.value = "";
        this.sendToParent("");
    }

    render() {
        return (
            <div id="customsearchfield" className="block-wrapper">
                <input id="customsearchinput" ref="searchfield" type="text" onKeyUp={ (event => this.handleSearchUpdate(event)).bind(this) } placeholder="Sök efter filer..."/>
                <div id="resetsearchfield" onClick={ this.handleResetSearchField }></div>
            </div>
        )
    }
}

export { Searchfield };