import React from 'react'
import { UPDATE_SEARCH } from '../actions/searchActions'

export default function reducer(state='', { type, payload }) {
    switch(type) {
        case UPDATE_SEARCH: {
            console.log("RETURNING FROM SEARCH REDUCER");
        	return {...state, searchTerm: payload}
        }
        default:
            return state;
    }
    
}