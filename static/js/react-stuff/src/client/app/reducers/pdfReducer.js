import React from 'react'
import { FETCH_PDFS } from '../actions/pdfActions'

export default function reducer(state='', { type, payload }) {
    switch(type) {
        case FETCH_PDFS: {
            console.log("RETURNING FROM PDF REDUCER");
        	return {...state, pdfs: payload}
        }
        default:
            return state;
    }
}