import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { combineReducers, createStore } from 'redux'
import { App } from './components/App'
import store from "./store"

const app = document.getElementById('react-app')

render(
    <Provider store={store}>
        <App/>
    </Provider>, app
);