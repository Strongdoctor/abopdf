import { createStore, applyMiddleware } from 'redux'
import promiseMiddleware from 'redux-promise';
import reducer from './reducers'

const initialState = {
	pdfs: { pdfs: [] },
	searchTerm: { searchTerm: ""},
};

const store = createStore(
	reducer,
	initialState,
	applyMiddleware(promiseMiddleware)
);

console.log(store.getState());

export default store;
