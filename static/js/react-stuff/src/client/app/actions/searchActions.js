export const UPDATE_SEARCH = 'searchTerm:updateSearch';

export function updateSearch(searchTerm) {
	console.log("UPDATED SEARCH")
	return {
		type: UPDATE_SEARCH,
		payload: searchTerm,
	}
}