import api_fetch from '../utils/api'

export const FETCH_PDFS = 'pdfs:fetchPdfs';

export function fetchPdfs() {
	const pdfs = api_fetch();
	console.log("FETCHED PDFS")
	return {
		type: FETCH_PDFS,
		payload: pdfs,
	}
}