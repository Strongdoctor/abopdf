var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

const in_development = true;

var config = {
    entry: {
        "bundle.min": APP_DIR + '/index.jsx',
    },
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(in_development ? 'development' : 'production')
        }),
    ],

    module: {
        loaders: [{
            test: /\.jsx?/,
            include: APP_DIR,
            loader: 'babel-loader',
        }]
    }
};

if(!in_development) {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            comments: false,

            compress: {
                warnings: false,
                drop_console: true,
            }
        })
    )
}

module.exports = config;
