const path = require('path');
const ASSET_PATH = process.env.ASSET_PATH || '/static/';

module.exports = {
    mode: 'development',
    entry: [
        "@babel/polyfill",
        "whatwg-fetch",
        path.resolve(__dirname, 'src/js/app/index.jsx')
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../static/js/'),
        publicPath: ASSET_PATH
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: { presets: ["@babel/env", "@babel/react"] }
                }
            },
            {
                test: /\.(woff2?|ttf|otf|eot|svg)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: ['style-loader','css-loader','resolve-url-loader','sass-loader']
            }
        ]
    }
};