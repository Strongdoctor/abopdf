import React, { Component } from 'react'
import PdfTable from './PdfTable'

class App extends Component {
  render() {
    return <PdfTable />;
  }
}

export { App };