import React, { Component } from 'react'
import {render} from 'react-dom'
import { connect } from "react-redux"
import { fetchPdfs } from "../actions/pdfActions"
import { updateSearch } from "../actions/searchActions"
import { Searchfield } from "./Searchfield"
import { PdfTableRow } from "./PdfTableRow"

class PdfTable extends Component {
    constructor(props) {
        super(props);
        this.onFetchPdfs = this.onFetchPdfs.bind(this);
        this.onUpdateSearch = this.onUpdateSearch.bind(this);
    }

    onFetchPdfs() {
        this.props.onFetchPdfs();
    }

    onUpdateSearch(searchTerm) {
        this.props.onUpdateSearch(searchTerm);
    }

    myCallback = (dataFromChild) => {
        this.props.onUpdateSearch(dataFromChild);
    }

    componentDidMount() {
        this.onFetchPdfs();
    }

    render() {
        console.log("THIS.PROPS:")
        console.log(this.props);

        const filterStr = this.props.searchTerm.searchTerm.toLowerCase()
        var filterArr = filterStr.split(" ");

        const pdfs = this.props.pdfs.pdfs;
        console.log("PDFS:")
        console.log(pdfs)

        filterArr = filterArr.filter(function(n){ return (n != undefined && n != "" && n != null)});
        var filteredElements = pdfs.slice();

        for (let i = 0; i < filterArr.length; i++) {
            const filterTerm = filterArr[i];

            filteredElements = filteredElements.filter(element => {
                const toSearchIn_title = element.title.toLowerCase();
                const toSearchIn_faculty = element.faculty.toLowerCase();
                const toSearchIn_pdf_type = element.pdf_type.toLowerCase();

                if (toSearchIn_title.includes(filterTerm)) { return element }
                if (toSearchIn_faculty.includes(filterTerm)) { return element }
                if (toSearchIn_pdf_type.includes(filterTerm)) { return element }
            });
        }
        

        if (filteredElements.length < 1 && filterStr) {
            filteredElements = "Hittade ingenting! :("
        } else {
            for(let i = 0; i < filteredElements.length; i++) {
                const element = filteredElements[i];

                filteredElements[i] = PdfTableRow({
                    idx: i,
                    pdf_link: element.pdf_link,
                    title: element.title,
                    faculty: element.faculty,
                    pdf_type: element.pdf_type,
                    date: element.date,
                })
            }
        }

        console.log("RERENDERING!");
        return(

            <div>
                <Searchfield callbackFromParent={this.myCallback}/>
                <table id="result-table">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Fakultet</th>
                            <th>Typ</th>
                        </tr>
                    </thead>
                    <tbody>
                        { filteredElements }
                    </tbody>
                </table>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    pdfs: state.pdfs,
    searchTerm: state.searchTerm,
});

const mapActionsToProps = {
    onFetchPdfs: fetchPdfs,
    onUpdateSearch: updateSearch,
};

export default connect(mapStateToProps, mapActionsToProps)(PdfTable);