import { combineReducers } from 'redux';

import pdfReducer from './pdfReducer'
import searchReducer from './searchReducer'

export default combineReducers({
	pdfs: pdfReducer,
	searchTerm: searchReducer,
})